import React, { Component } from 'react'
import { BrowserRouter, Route, Link } from 'react-router-dom';
import queryString from 'query-string';
import Auth from './auth';
import Root from './root';

const App = () => (
  <BrowserRouter>
    <div>
      <Route exact path='/' component={Root} />
      <Route path='/auth' render={ (props) => <Auth qs={queryString.parse(props.location.search)} />} />
    </div>
  </BrowserRouter>
)

export default App