import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import axios from 'axios';

// こちらで用意したサーバのエンドポイント
const url = "http://127.0.0.1:8000" + "/oauth";

// OAuth認証承認後、このページに遷移する
let Auth = (props) => {
  const [user, setUser] = useState({
    id: null,
    username: null,
    avatar_url: null,
    name: null
  });

  const qs = props.qs;

// このコンポーネントがマウントされたときにサーバへGitLabが発行したcode, stateを送る
let Auth = (props) => {
  useEffect(() => {
    getAuth()
  }, []);

  // このコンポーネントがマウントされたときにサーバへGitLabが発行したcode, stateを送る
  let getAuth = () => {
    let params = new URLSearchParams();

    // redirect uriにクエリパラメータとして付いているcodeとstateをサーバへ送る
    params.append('code', qs.code);
    params.append('state', qs.state);
    axios.post(url, params)
    .then(response => {
      setUser(response.data);
    }).catch(error => {
      console.log(error);
    });
  }

  // 発行されたトークンを使って取得したユーザ情報を表示する
  return (
    <div>
      <img border="0" src={user.avatar_url} width="128" height="128" alt="avatar"></img>
      <h2>ID</h2>
      <p>{user.id}</p>
      <h2>Username</h2>
      <p>{user.username}</p>
      <h2>name</h2>
      <p>{user.name}</p>
      <Link to="/">Go back Home</Link>
    </div>
  );
}

export default Auth