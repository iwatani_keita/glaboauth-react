import React, { useState, useEffect } from 'react';

const appId = "xxxxx";
const scope = "api+profile+read_user";
const redirect_uri = encodeURI("http://127.0.0.1:3000/auth");
const oauth_url = "https://gitlab.com/oauth/authorize?client_id=" + appId + "&redirect_uri=" + redirect_uri + "&response_type=code&state=hoge&scope=" + scope;

// GitLabへのリンクを作成する
// 認証が完了するとredirect_uriに指定したエンドポイントにリダイレクトする
// このサンプルでは同Reactアプリの/authに遷移する
let Root = (props) => {
  return (
    <div>
      <h2>Home</h2>
      <p>Welcome to test oauth</p>
      <a href={oauth_url}><button>Sign in with GitLab</button></a>
    </div>
  );
}

export default Root